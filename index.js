export { default as AreaGraph } from './src/components/AreaGraph.vue';
export { default as Donut } from './src/components/DounetChart.vue';
export { default as BoxChart } from './src/components/LeftBox.vue';